package config;
 
import java.util.concurrent.TimeUnit;
import static executionEngine.DriverScript.OR;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import executionEngine.DriverScript;
import utilities.Constant;
import utilities.Utility;
import utilities.Log;
 
public class ActionKeywords {
 
		public static WebDriver driver;
		public static WebElement element;
		public static Select selection;
		private static int implicitWaitTime = 10;
 
	public static void openBrowser(String object, String data){		
				
		try{
			if(data.equals("Mozilla"))
			{
				driver = new FirefoxDriver();
				Log.info("Opening Firefox Browser");
			}
			if(data.equals("IE"))
			{
				driver = new InternetExplorerDriver();
				Log.info("Opening Internet Explorer Browser");
			}
			if(data.equals("Chrome"))
			{
		    	Utility.chromeSetup(driver);
				driver = new ChromeDriver();
				Log.info("Opening Chrome Browser");
			}
			driver.manage().timeouts().implicitlyWait(implicitWaitTime, TimeUnit.SECONDS);
		}
		catch (Exception e)
		{
			Log.info("Bowser not opened or error in driver initialization.. Refer to ActionKeywords : line 52");
			throw(e);
		}
		}
 
	public static void navigate(String object, String data){	
		try{
		driver.get(Constant.mercuryToursURL);
		Log.info("Navigating to website");
		}catch(Exception e)
		{
			Log.info("Website not found or address is incorrect.. Refer to ActionKeywords : line 63");
			try {
				DriverScript.writeToCell(Constant.KEYWORD_FAIL, DriverScript.iTestStep, Constant.Col_TestStepResult, Constant.Test_Steps_Sheet);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			driver.quit();
			throw(e);
		}
		}
 
	public static void input(String object, String data){
		
		try{
		element = driver.findElement(By.xpath(OR.getProperty(object))); 
		element.clear();
		element.sendKeys(data);
		}catch (Exception e)
		{
			Log.info("Element not found or xpath is incorrect.. Refer to ActionKeywords : line 78");
			try {
				DriverScript.writeToCell(Constant.KEYWORD_FAIL, DriverScript.iTestStep, Constant.Col_TestStepResult, Constant.Test_Steps_Sheet);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			driver.quit();
			throw(e);
		}
		}
 
	public static void radio_type(String object, String data)
	{
//		//try{
//		selection = new Select(
//				driver.findElement(By.xpath(OR.getProperty(object))));
//		selection.selectByVisibleText(data);
//		
//			Log.info("selecting " + data);
//		//}catch(Exception e)
//		//{
//			Log.info("Element not found or name is incorrect.. Refer to ActionKeywords : line 104");
//			//throw(e);
//		//}
	}
	
	public static void click_button(String object, String data){
		try{
		element = driver.findElement(By.xpath(OR.getProperty(object)));
		Log.info("Clicking Login Button");
		element.click();
		}catch(Exception e)
		{
			Log.info("Element not found or name is incorrect.. Refer to ActionKeywords : line 107");
			try {
				DriverScript.writeToCell(Constant.KEYWORD_FAIL, DriverScript.iTestStep, Constant.Col_TestStepResult, Constant.Test_Steps_Sheet);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			driver.quit();
			throw(e);
		}
		}
 
	public static void select(String object, String data)
	{	
		try{
		selection = new Select(
				driver.findElement(By.xpath(OR.getProperty(object))));
		
		selection.selectByVisibleText(data);
		
			Log.info("selecting " + data);
		}catch(Exception e)
		{
			Log.info("Invalid entry.. Refer to ActionKeywords : line 125");
			try {
				DriverScript.writeToCell(Constant.KEYWORD_FAIL, DriverScript.iTestStep, Constant.Col_TestStepResult, Constant.Test_Steps_Sheet);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			driver.quit();
			throw(e);
		}
		checkAlert();
	}
	
	public static void service_class(String object, String data)
	{
		try{
		element = driver.findElement(By.xpath(OR.getProperty(object)));
		Log.info("selecting " + data);
		element.click();
		}catch(Exception e)
		{
			Log.info("Element not found or name is incorrect.. Refer to ActionKeywords : line 141");
			try {
				DriverScript.writeToCell(Constant.KEYWORD_FAIL, DriverScript.iTestStep, Constant.Col_TestStepResult, Constant.Test_Steps_Sheet);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			driver.quit();
			throw(e);
		}
	}
	
	public static void select_outflight(String object, String data)
	{
		try{
		element = driver.findElement(By.xpath(OR.getProperty(object)));
		Log.info("selecting " + data);
		element.click();
		}catch(Exception e)
		{
			Log.info("Element not found or name is incorrect.. Refer to ActionKeywords : line 156");
			try {
				DriverScript.writeToCell(Constant.KEYWORD_FAIL, DriverScript.iTestStep, Constant.Col_TestStepResult, Constant.Test_Steps_Sheet);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			driver.quit();
			throw(e);
		}
	}
	
	public static void select_inflight(String object, String data)
	{
		try{
		element = driver.findElement(By.xpath(OR.getProperty(object)));
		Log.info("selecting " + data);
		element.click();
		}catch(Exception e)
		{
			Log.info("Element not found or name is incorrect.. Refer to ActionKeywords : line 171");
			try {
				DriverScript.writeToCell(Constant.KEYWORD_FAIL, DriverScript.iTestStep, Constant.Col_TestStepResult, Constant.Test_Steps_Sheet);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			driver.quit();
			throw(e);
		}
	}
	
	public static void closeBrowser(String object, String data){
		Log.info("Closing Browser");
			driver.quit();
		}
 
	public static void checkAlert() {
	    try {
	        WebDriverWait wait = new WebDriverWait(driver, 1);
	        wait.until(ExpectedConditions.alertIsPresent());
	        Alert alert = driver.switchTo().alert();
	        alert.accept();
	    } catch (Exception e) {
	        //exception handling
	    }
	}
	}

