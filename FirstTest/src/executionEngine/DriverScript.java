package executionEngine;
 
import java.io.FileInputStream;
import java.lang.reflect.Method;
import java.util.Properties;

import org.apache.log4j.xml.DOMConfigurator;

import config.ActionKeywords;
import utilities.Constant;
import utilities.ExecUtil;
import utilities.Log;
 
public class DriverScript {
 
	public static Properties OR;
	public static ActionKeywords actionKeywords;
	public static String sActionKeyword;
	public static String sPageObject;
	public static Method method[];
	
	public static int iTestStep;
	public static int iTestLastStep;
	public static String sTestCaseID;
	public static String sRunMode;
	public static String sData;
 
	public DriverScript() throws NoSuchMethodException, SecurityException{
		actionKeywords = new ActionKeywords();
		method = actionKeywords.getClass().getMethods();
		
	}
 
	//public static boolean bResult = true;
	
    public static void main(String[] args) throws Exception {
    	
    	String Path_DataEngine = Constant.DataEngine_Excel_Path;
    	ExecUtil.setExcelFile(Path_DataEngine, Constant.Test_Steps_Sheet);

    	DOMConfigurator.configure("log4j.xml");
    	
		//Declaring String variable for storing Object Repository path
    	String Path_OR = Constant.Path_OR;
		//Creating file system object for Object Repository text/property file
		FileInputStream fs = new FileInputStream(Path_OR);
		//Creating an Object of properties
		OR= new Properties(System.getProperties());
		//Loading all the properties from Object Repository property file in to OR object
		OR.load(fs);
		
		DriverScript startEngine = new DriverScript();
		startEngine.execute_TestCase();
 
    	}

    
    private void execute_TestCase() throws Exception {       	
		//This will return the total number of test cases mentioned in the Test cases sheet
    	int iTotalTestCases = ExecUtil.getRowCount(Constant.Test_Cases_Sheet);
    	
		//This loop will execute number of times equal to Total number of test cases
		for(int iTestcase=1;iTestcase<=iTotalTestCases;iTestcase++){
			
			//This is to get the Test case name from the Test Cases sheet
			sTestCaseID = ExecUtil.getCellData(iTestcase, Constant.Col_Test_Case_ID, Constant.Test_Cases_Sheet);
			
			//This is to get the value of the Run Mode column for the current test case
			sRunMode = ExecUtil.getCellData(iTestcase, Constant.Col_RunMode, Constant.Test_Cases_Sheet);
			
			//This is the condition statement on RunMode value
			if (sRunMode.equals("Yes")){
				
				//Only if the value of Run Mode is 'Yes', this part of code will execute
				iTestStep = ExecUtil.getRowContains(sTestCaseID, Constant.Col_Test_Case_ID, Constant.Test_Steps_Sheet);
				iTestLastStep = ExecUtil.getTestStepsCount(Constant.Test_Steps_Sheet, sTestCaseID, iTestStep);
				

				Log.startTestCase(sTestCaseID);
								
				//This loop will execute number of times equal to Total number of test steps
				for (; iTestStep <= 45; iTestStep++){
		    		sActionKeyword = ExecUtil.getCellData(iTestStep, Constant.Col_ActionKeyword, Constant.Test_Steps_Sheet);
		    		sPageObject = ExecUtil.getCellData(iTestStep, Constant.Col_PageObject_ID, Constant.Test_Steps_Sheet);
		    		sData = ExecUtil.getCellData(iTestStep, Constant.Col_DataSet, Constant.Test_Steps_Sheet);
		    		
		    		execute_Actions();
		    		
//					if(bResult == false){
//						ExecUtil.setCellData(Constant.KEYWORD_FAIL, iTestStep, Constant.Col_TestStepResult, Constant.Test_Steps_Sheet);
//						break;
//						}		
//					if(bResult == true){
//						ExecUtil.setCellData(Constant.KEYWORD_PASS, iTestStep, Constant.Col_TestStepResult, Constant.Test_Steps_Sheet);
//						}	
					}
			}
		}
			
	Log.endTestCase(sTestCaseID);
    }
 
    public static void writeToCell(String result, int row, int col, String sheet) throws Exception
    {
    	ExecUtil.setCellData(result, row, col, sheet);
    }
    
     private static void execute_Actions() throws Exception {
    	     	 	 
		for(int i=0;i<method.length;i++){		
			if(method[i].getName().equals(sActionKeyword)){				
				method[i].invoke(actionKeywords, sPageObject, sData);
				break;
				}
			}
		}
 }