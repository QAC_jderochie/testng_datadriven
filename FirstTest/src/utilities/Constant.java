package utilities;

public class Constant {

	public static final String URL = "http://www.canada411.ca/";
	public static final String mercuryToursURL = "http://newtours.demoaut.com/";
	public static final String DataEngine_Excel_Path = System.getProperty("user.dir") + "\\DataEngine.xlsx";
	public static final String ChromeDriver_Exe_Path = System.getProperty("user.dir") + "\\Deps\\ChromeDriver\\chromedriver.exe";
	public static final String Excel_Data_Path = System.getProperty("user.dir") + "\\Canada411.xlsx";
    public static final String Path_OR = System.getProperty("user.dir") + "\\OR.txt";
	
	
	public static final int Col_Test_Case_ID = 0;
	public static final int Col_TestScenario_ID = 1;
	public static final int Col_PageObject_ID = 4;
	public static final int Col_ActionKeyword = 5;
	
	public static final int Col_RunMode = 2;

	public static final int Col_Result = 3;
	public static final int Col_TestStepResult = 7;
	
	public static final String Test_Steps_Sheet = "Test Steps";
	public static final String Test_Cases_Sheet = "Test Cases";
	public static final String UserName = "roneill";
	public static final String Password = "Defcon4]";
	
	
	public static final String KEYWORD_FAIL = "FAIL";
	public static final String KEYWORD_PASS = "PASS";
	
	public static final int Col_DataSet = 6;
}
