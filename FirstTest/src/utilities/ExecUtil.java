package utilities;

import java.io.FileInputStream;
import java.io.FileOutputStream;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import executionEngine.DriverScript;

public class ExecUtil {
	private static XSSFSheet ExcelWSheet;
	private static XSSFWorkbook ExcelWBook;
	private static XSSFCell Cell;
	private static XSSFRow Row;
	
	// This method is to set the File Path and to open the excel file, Pass Excel path and sheetname as arguments to this method
	public static void setExcelFile(String path, String SheetName) throws Exception
	{
		try{
			// Open the excel file
			FileInputStream ExcelFile = new FileInputStream(path);
			
			// Access the required test data sheet
			ExcelWBook = new XSSFWorkbook(ExcelFile);
			
			ExcelWSheet = ExcelWBook.getSheet(SheetName);
			
		}catch(Exception e)
		{
			throw(e);
		}
	}
	
	// This method is to read the test data from the excel cell, in this we are passing parameters as Row num and Col num
	public static String getCellData(int RowNum, int ColNum, String sheetName) throws Exception
	{
		try{
			Cell = ExcelWSheet.getRow(RowNum).getCell(ColNum);
			
			DataFormatter formatter = new DataFormatter();
			String CellData = formatter.formatCellValue(Cell);
			
			return CellData;
		}
		catch(Exception e)
		{
			return "";
		}
	}
	
	//This method is to get the row count used of the excel sheet
	public static int getRowCount(String SheetName){
			ExcelWSheet = ExcelWBook.getSheet(SheetName);
			int number=ExcelWSheet.getLastRowNum()+1;
			return number;
		}
	
	//This method is to get the Row number of the test case
	//This methods takes three arguments(Test Case name , Column Number & Sheet name)
	public static int getRowContains(String sTestCaseName, int colNum,String SheetName) throws Exception{
		int i;	
		ExcelWSheet = ExcelWBook.getSheet(SheetName);
			int rowCount = ExecUtil.getRowCount(SheetName);
			for (i = 0 ; i < rowCount; i++){
				if (ExecUtil.getCellData(i, colNum, SheetName).equalsIgnoreCase(sTestCaseName)){
					break;
				}
			}
			return i;
			}
	
	//This method is to get the count of the test steps of test case
	//This method takes three arguments (Sheet name, Test Case Id & Test case row number)
	public static int getTestStepsCount(String SheetName, String sTestCaseID, int iTestCaseStart) throws Exception{
		for(int i = iTestCaseStart; i <= ExecUtil.getRowCount(SheetName); i++){
			if(!sTestCaseID.equals(ExecUtil.getCellData(i, Constant.Col_Test_Case_ID, SheetName))){
				int number = i;
				return number;
			}
		}
		ExcelWSheet = ExcelWBook.getSheet(SheetName);
		int number = ExcelWSheet.getLastRowNum()+1;
		return number;
	}
	
	
	@SuppressWarnings("static-access")
	public static void setCellData(String Result,  int RowNum, int ColNum, String SheetName) throws Exception    {
           try{

        	   ExcelWSheet = ExcelWBook.getSheet(SheetName);
               Row  = ExcelWSheet.getRow(RowNum);
               Cell = Row.getCell(ColNum, Row.RETURN_BLANK_AS_NULL);
               if (Cell == null) {
            	   Cell = Row.createCell(ColNum);
            	   Cell.setCellValue(Result);
                } else {
                    Cell.setCellValue(Result);
                }
               
                 FileOutputStream fileOut = new FileOutputStream(Constant.DataEngine_Excel_Path);
                 ExcelWBook.write(fileOut);
                 //fileOut.flush();
                 fileOut.close();
                 ExcelWBook = new XSSFWorkbook(new FileInputStream(Constant.DataEngine_Excel_Path));
             }catch(Exception e){

             }
        }
}
