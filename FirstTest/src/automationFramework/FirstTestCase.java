package automationFramework;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;


public class FirstTestCase {

	static WebDriver driver;
	static boolean earliest;
	static boolean lowest;
	
	public static void main(String[] args) throws InterruptedException 
	{
		System.setProperty("webdriver.chrome.driver",  "C:\\Users\\jderochie\\Desktop\\chromeDriver\\chromedriver.exe");
		driver = new ChromeDriver();
		System.out.println("Successfully opened the browser");
		
		// Open the webpage
		openPage("http://newtours.demoaut.com");
		
		// Get the window title
		String bob = getTitle();
		System.out.println("The Window Title is : " + bob);
		
		//// Fail at login
		login("roneill", "bobby23[");
		
		openPage("http://newtours.demoaut.com");
		
		// Register 5 users on the site
		registerUsers("Jesse", "Derochie", "3453456", "jesse@jesse.com", "321 French Rd.", "Apartment B", "Etobicoke", "Ontario", "K0L2S0", "CANADA", "Jesse", "54321");
		//driver.wait(5);
		openPage("http://newtours.demoaut.com");
		registerUsers("Bob", "Bobsworth", "2345678", "bob@bob.com", "321 English Rd.", "Apartment A", "Frederickton", "New Brunswick", "J012D3", "CANADA", "Bob", "12345");
		//driver.wait(5);
		openPage("http://newtours.demoaut.com");
		registerUsers("Frank", "Franklin", "9876543", "frank@frank.com", "654 Welsh Rd.", "PO Box 32", "Regina", "Saskatchewan", "p4g3f9", "CANADA", "Frank", "912827");
		//driver.wait(5);
		openPage("http://newtours.demoaut.com");
		registerUsers("Jill", "Jillings", "3847564", "jill@jill.com", "8764 Centennial Dr.", " ", "Vancouver", "British Columbia", "e0r3hd", "CANADA", "Jill", "20973");
		//driver.wait(5);
		openPage("http://newtours.demoaut.com");
		registerUsers("Sally", "Salippers", "9874637", "sally@sally.com", "1 Meryl Way", "", "Markham", "Ontario", "n3n0s9", "CANADA", "Sally", "4524545");
		//driver.wait(5);
		openPage("http://newtours.demoaut.com");

		
		// login to the website
		login("roneill", "Defcon4]");
		//driver.wait(5);
		
		// do we want the earliest flight, or the cheapest flight
		setFlightChoiceConditions(true, false);
		
		// Book a flight
		bookFlight("OneWay", "3", "New York", "August", "21", "Zurich", "September", "28", "First", "Pangea Airlines");
		//driver.wait(5);
		
		// Random Unique information about guests 
		List guests = new ArrayList<String>();
		guests.add("Bobsworth"); guests.add("B"); guests.add("Bobby");
		guests.add("Sally"); guests.add("Herbert");
		
		List meals = new ArrayList<String>();
		meals.add("Diabetic"); meals.add("Bland"); meals.add("Kosher");
		
		List creditCard = new ArrayList<String>();
		creditCard.add("Visa"); creditCard.add("123456789101"); creditCard.add("10"); creditCard.add("2010");
		
		List billAddress = new ArrayList<String>();
		billAddress.add("72 French Rd"); billAddress.add("Dellville"); billAddress.add("Ontario"); billAddress.add("90210"); billAddress.add("CANADA");
		
		List shipAddress = new ArrayList<String>();
		shipAddress.add("23409 Dirt Rd"); shipAddress.add("Bird"); shipAddress.add("Pacu Pacu"); shipAddress.add("01209"); shipAddress.add("SWEDEN");
		
		// Reserve the tickets for the guests
		reservationsAndPayment(guests, meals, creditCard, billAddress, shipAddress);
		//driver.wait(5);
		
		driver.wait(25);
		
		System.out.println("Successfully closed the browser");
		// close the driver
		driver.close();
	}

	private static void openPage(String webPage)
	{
		driver.get(webPage);
	}
	
	private static String getTitle()
	{
		return driver.getTitle();
	}
	
	private static void login(String userName, String password)
	{
		WebElement element;
		element = driver.findElement(By.name("userName"));
		element.sendKeys("roneill");
		
		element = driver.findElement(By.name("password"));
		element.sendKeys("Defcon4]");
		
		element = driver.findElement(By.name("login"));
		element.click();
	}
	
	private static void setFlightChoiceConditions(boolean earliestFlight, boolean lowestCost)
	{
		earliest = earliestFlight;
		lowest = lowestCost;
	}
	
	private static void bookFlight(
			String triptype, 
			String numPassengers, 
			String leavingFrom, 
			String leavingMonth, 
			String leavingDay, 
			String destination, 
			String arrivalMonth, 
			String arrivalDay, 
			String flightClass, 
			String airline)
	{
		List<WebElement> element;
		element = driver.findElements(By.name("tripType"));
		
		// if the tripType is one way, check if its selected, if not select it
		if(triptype == "OneWay" && !element.get(1).isSelected())
		{
			element.get(1).click();
		}
		// if the triptype is round trip, check if its selected if not select it
		if(triptype == "RoundTrip" && !element.get(0).isSelected())
		{
			element.get(0).click();
		}
		
		// How many passengers
		Select passengerSelection = new Select(
				driver.findElement(By.xpath("html/body/div[1]/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td/form/table/tbody/tr[3]/td[2]/b/select")));
		passengerSelection.selectByVisibleText(numPassengers);
		
		// Where are you leaving from
		Select leavingSelection = new Select(
				driver.findElement(By.xpath("html/body/div[1]/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td/form/table/tbody/tr[4]/td[2]/select")));
		leavingSelection.selectByVisibleText(leavingFrom);
		
		// what month are you leaving
		Select leaveMonthSelection = new Select(
				driver.findElement(By.xpath("html/body/div[1]/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td/form/table/tbody/tr[5]/td[2]/select[1]")));
		leaveMonthSelection.selectByVisibleText(leavingMonth);
		
		// what day are you leaving
		Select leaveDaySelection = new Select(
				driver.findElement(By.xpath("html/body/div[1]/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td/form/table/tbody/tr[5]/td[2]/select[2]")));
		leaveDaySelection.selectByVisibleText(leavingDay);
		
		// what is the destination
		Select destinationSelection = new Select(
				driver.findElement(By.xpath("html/body/div[1]/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td/form/table/tbody/tr[6]/td[2]/select")));
		destinationSelection.selectByVisibleText(destination);
		
		// arrival month
		Select arrMonthSelection = new Select(
				driver.findElement(By.xpath("html/body/div[1]/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td/form/table/tbody/tr[7]/td[2]/select[1]")));
		arrMonthSelection.selectByVisibleText(arrivalMonth);
		
		// arrival day
		Select arrDaySelection = new Select(
				driver.findElement(By.xpath("html/body/div[1]/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td/form/table/tbody/tr[7]/td[2]/select[2]")));
		arrDaySelection.selectByVisibleText(arrivalDay);

		// what flight class was chosen
		element = driver.findElements(By.name("servClass"));
		
		// if the servClass is Coach, check if its selected, if not select it
		if(flightClass == "Coach" && !element.get(0).isSelected())
		{
			element.get(0).click();
		}
		// if the servClass is Business, check if its selected if not select it
		if(flightClass == "Buisness" && !element.get(1).isSelected())
		{
			element.get(1).click();
		}
		// if the servClass is First, check if its selected if not select it
		if(flightClass == "First" && !element.get(2).isSelected())
		{
			element.get(2).click();
		}
		
		// what airline was selected
		Select airLineSelection = new Select(
				driver.findElement(By.xpath("html/body/div[1]/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td/form/table/tbody/tr[10]/td[2]/select")));
		airLineSelection.selectByVisibleText(airline);
		
		WebElement singleElement = driver.findElement(By.name("findFlights"));
		singleElement.click();
		
		element = driver.findElements(By.name("outFlight"));
		
		if(airline == "Blue Skies Airlines")
		{
			// determine cheaper flight? 
			// or determine earliest or latest flight?
			if(lowest)
			{
				element.get(0).click();
			}
			else if(earliest)
			{
				element.get(1).click();
			}

		}
		else if(airline == "Pangea Airlines")
		{
			element.get(2).click();
		}
		else if(airline == "Unified Airlines")
		{
			element.get(3).click();
		}
		
		element = driver.findElements(By.name("inFlight"));
		
		if(airline == "Blue Skies Airlines")
		{
			// determine cheaper flight? 
			// or determine earliest or latest flight?
			if(lowest)
			{
				element.get(0).click();
			}
			else if(earliest)
			{
				element.get(1).click();
			}
		}
		else if(airline == "Pangea Airlines")
		{
			element.get(2).click();
		}
		else if(airline == "Unified Airlines")
		{
			element.get(3).click();
		}
		
		singleElement = driver.findElement(By.name("reserveFlights"));
		singleElement.click();
	}
	
	private static void reservationsAndPayment(
List<String> guestNames, 
List<String> mealPreferance, 
List<String> cardData,  
List<String> billingAddress, 
List<String> shippingAddress)
	{
		WebElement element;
		Select selector;
		
		// Set up guest names
		for (int i = 0; i < guestNames.size(); i++){
			if(i == 3)
			{
				break;
			}
			element = driver.findElement(By.name("passFirst" + i));
			element.sendKeys(guestNames.get(i + 2));
			element = driver.findElement(By.name("passLast" + i));
			element.sendKeys(guestNames.get(0));
		}
		
		// Set up meal preferances
		selector = new Select(
				driver.findElement(
						By.xpath("html/body/div[1]/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td/form/table/tbody/tr[4]/td/table/tbody/tr[2]/td[3]/select")));
		selector.selectByVisibleText(mealPreferance.get(0));
		
		selector = new Select(
				driver.findElement(
						By.xpath("html/body/div[1]/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td/form/table/tbody/tr[5]/td/table/tbody/tr[2]/td[3]/select")));
		selector.selectByVisibleText(mealPreferance.get(1));
		
		selector = new Select(
				driver.findElement(
						By.xpath("html/body/div[1]/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td/form/table/tbody/tr[6]/td/table/tbody/tr[2]/td[3]/select")));
		selector.selectByVisibleText(mealPreferance.get(2));
		
		
		///// NEED TO CLEAR THE CONTENTS OF THE FIELDS BEFORE YOU FILL THEM
		
		// Fill in Credit card Data
		// Card vendor
		selector = new Select(
				driver.findElement(
						By.xpath("html/body/div[1]/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td/form/table/tbody/tr[8]/td/table/tbody/tr[2]/td[1]/select")));
		
		selector.selectByVisibleText(cardData.get(0));
		
		// Card Number
		element = driver.findElement(By.name("creditnumber"));
		element.sendKeys(cardData.get(1));
		
		// Card Expiry Month
		selector = new Select(
				driver.findElement(
						By.xpath("html/body/div[1]/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td/form/table/tbody/tr[8]/td/table/tbody/tr[2]/td[3]/select[1]")));
		
		
		selector.selectByVisibleText(cardData.get(2));
		
		// Card Expiry Year
		selector = new Select(
				driver.findElement(
						By.xpath("html/body/div[1]/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td/form/table/tbody/tr[8]/td/table/tbody/tr[2]/td[3]/select[2]")));
		
		selector.selectByVisibleText(cardData.get(3));
		
		// Card Holder First Name
		element = driver.findElement(By.name("cc_frst_name"));
		element.sendKeys(guestNames.get(2));
		
		// Card holder Second Name
		element = driver.findElement(By.name("cc_mid_name"));
		element.sendKeys(guestNames.get(1));
		
		// Card holder Third Name
		element = driver.findElement(By.name("cc_last_name"));
		element.sendKeys(guestNames.get(0));
		
		// Billing Address
		element = driver.findElement(By.name("billAddress1"));
		driver.findElement(By.name("billAddress1")).clear();
		element.sendKeys(billingAddress.get(0));
		
		element = driver.findElement(By.name("billCity"));
		driver.findElement(By.name("billCity")).clear();
		element.sendKeys(billingAddress.get(1));
		
		element = driver.findElement(By.name("billState"));
		driver.findElement(By.name("billState")).clear();
		element.sendKeys(billingAddress.get(2));
		
		element = driver.findElement(By.name("billZip"));
		driver.findElement(By.name("billZip")).clear();
		element.sendKeys(billingAddress.get(3));
		
		selector = new Select(
				driver.findElement(
						By.xpath("html/body/div[1]/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td/form/table/tbody/tr[15]/td[2]/select")));
		
		
		selector.selectByVisibleText(billingAddress.get(4));
		
		// Shipping Address
		element = driver.findElement(By.name("delAddress1"));
		driver.findElement(By.name("delAddress1")).clear();
		element.sendKeys(shippingAddress.get(0));
		
		element = driver.findElement(By.name("delCity"));
		driver.findElement(By.name("delCity")).clear();
		element.sendKeys(shippingAddress.get(1));
		
		element = driver.findElement(By.name("delState"));
		driver.findElement(By.name("delState")).clear();
		element.sendKeys(shippingAddress.get(2));
		
		element = driver.findElement(By.name("delZip"));
		driver.findElement(By.name("delZip")).clear();
		element.sendKeys(shippingAddress.get(3));
		
		selector = new Select(
				driver.findElement(
						By.xpath("html/body/div[1]/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td/form/table/tbody/tr[21]/td[2]/select")));
	
		selector.selectByVisibleText(shippingAddress.get(4));
		
		element = driver.findElement(By.name("buyFlights"));
		element.click();
		
		
	}
	
	private static void registerUsers(
			String firstName, 
			String lastName, 
			String phoneNumber, 
			String emailAddress, 
			String addressLine1, 
			String addressLine2,
			String city,
			String state, 
			String postalCode, 
			String country, 
			String userName, 
			String passWord)
	{

		WebElement registerButton;
		registerButton = driver.findElement(By.linkText("REGISTER"));
		registerButton.click();
		
		// fill in the first name
		WebElement element;
		element = driver.findElement(By.name("firstName"));
		element.sendKeys(firstName);
		
		// fill in the last name
		element = driver.findElement(By.name("lastName"));
		element.sendKeys(lastName);
		
		// fill in the phone number
		element = driver.findElement(By.name("phone"));
		element.sendKeys(phoneNumber);
		
		// fill in the email field
		element = driver.findElement(By.name("userName"));
		element.sendKeys(emailAddress);
		
		// fill in the 1st address field
		element = driver.findElement(By.name("address1"));
		element.sendKeys(addressLine1);
		
		// fill in the 2nd address field 
		element = driver.findElement(By.name("address2"));
		element.sendKeys(addressLine2);
		
		// fill in the city field
		element = driver.findElement(By.name("city"));
		element.sendKeys(city);
		
		// fill in the state field
		element = driver.findElement(By.name("state"));
		element.sendKeys(state);
		
		// fill in the postal code field
		element = driver.findElement(By.name("postalCode"));
		element.sendKeys(postalCode);
		
		// select the country 
		Select countrySelection = new Select(
				driver.findElement(By.xpath("html/body/div[1]/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td/form/table/tbody/tr[12]/td[2]/select")));
		countrySelection.selectByVisibleText(country);
		
		// fill in the username field
		element = driver.findElement(By.name("email"));
		element.sendKeys(userName);
		
		// fill in the password field
		element = driver.findElement(By.name("password"));
		element.sendKeys(passWord);
		
		// fill in the confirm password field
		element = driver.findElement(By.name("confirmPassword"));
		element.sendKeys(passWord);
		
		// register this user
		element = driver.findElement(By.name("register"));
		element.click();
	}
	
}