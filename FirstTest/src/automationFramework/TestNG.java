//package automationFramework;
//
//import org.testng.annotations.Test;
//import org.testng.annotations.BeforeMethod;
//import org.testng.Assert;
//
//import java.util.Locale;
//import java.util.concurrent.TimeUnit;
//
//import org.apache.log4j.Logger;
//import org.apache.log4j.xml.DOMConfigurator;
//import org.openqa.selenium.By;
//import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebElement;
//import org.openqa.selenium.chrome.ChromeDriver;
//import org.testng.Reporter;
//import org.testng.annotations.AfterMethod;
//
//import utilities.Constant;
//import utilities.ExecUtil;
//import utilities.Utility;
//
//public class TestNG {
//	
//	private static WebDriver driver = null;
//	private static Logger Log = Logger.getLogger(TestNG.class.getName());
//	private static int index = 1;
//
//	  @Test
//	  public void f() throws Exception 
//	  {
//
//		/* 
//		   Since this test is based on searching for people using their given phone number,
//		   it is safe to assume that if there is no phone number in the first column, the list has
//		   been exhausted. Therefore a while loop continuously cycling until a blank cell is 
//		   found in that column, makes this class scalable in this test case.
//		*/
//		
//		while(!ExecUtil.getCellData(index, 0).equals(""))
//		{
//			// Load the webpage
//		     driver.get(Constant.URL);
//		     
//		    // Report to log4J for each test, but not to the TestNG Reporter
//		     Log.info("Website Opened Successfully");
//		  
//		     // Test whether the data that has been supplied by the excel file, matches the data 
//		     // on the website
//		     
//		    if(reverseLookUp(index, ExecUtil.getCellData(index, 0), ExecUtil.getCellData(index,  1), ExecUtil.getCellData(index,  2)))
//		    {
//		    	// This test passed, assign "PASS" to the corresponding cell
//		    	ExecUtil.setCellData("PASS", index, 4);
//		    	Log.info(ExecUtil.getCellData(index, 1) + " Test passed.");
//		    	Reporter.log(ExecUtil.getCellData(index, 1) + "Test Passed.");
//		    }
//		    else
//		    {
//		    	// This test failed, assign "FAIL" to the corresponding cell
//		    	ExecUtil.setCellData("FAIL", index, 4);
//		    	Log.warn(ExecUtil.getCellData(index, 1) + " Test failed.");
//		    	Reporter.log(ExecUtil.getCellData(index, 1) + "Test Failed.");
//		    }
//		    
//		    // increment the index
//			index++;
//			
//		}
//	  }
//  
//	  @BeforeMethod
//	  public void beforeMethod() throws Exception
//	  {
//		    // Configure log4j using the supplied xml
//			DOMConfigurator.configure("log4j.xml");
//			
//		    // Begin the Logging process
//		    Log.info("Log for " +  Utility.getCurrentTimeStamp());
//		    Reporter.log("Log beginning at : " + Utility.getCurrentTimeStamp());
//			
//			// Set the excel file
//		    ExecUtil.setExcelFile(Constant.Excel_Data_Path,"Sheet1");
//		    
//		    // Setup the chrome driver
//			Utility.chromeSetup(driver);
//			driver = new ChromeDriver();
//		    
//		    // Set wait period for driver
//		    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//		    
//		    // Notify the log that the driver has been set up
//		    if(driver != null)
//		    {
//		        Log.info("New driver instantiated");
//		        Reporter.log("New driver instantiated");
//		    }
//	  }
//  
//	  @AfterMethod
//	  public void afterMethod()
//	  {
//		  // Report to the logs that the tests have all completed
//		    Log.info("*******Program exiting safely, all tests complete.*******");
//		    Reporter.log("*******Program exiting safely, all tests complete.*******");
//		    
//		  // close the driver
//		    driver.close();
//	  }
//
//	  /*
//	   * This method will take the passed parameters and determines whether the information supplied
//	   * is valid or not.
//	   * @param row - The current row being tested
//	   * @param number - The phone number 
//	   * @param name - The Persons name
//	   * @param address - The Persons address
//	   * Throws Exception
//	   */
//	public boolean reverseLookUp(int row, String number, String name, String address) throws Exception
//	{
//		WebElement element;
//		
//		try{
//			    // Grab handle to reverse lookup field
//				element = driver.findElement(By.id("c411PeopleReverseWhat"));
//				
//				// enter the phone number
//				element.sendKeys(number);
//				
//				/* Use the following line to incur an Exception
//				  element = driver.findElement(By.name("bobthebuilder")); */
//				
//				// submit the query
//				element.submit();
//			
//				// Grab handle to name text box
//				element = driver.findElement(By.xpath(".//*[@id='contact']/h1"));
//				
//				// Test that the names match
//				// Assert.assertTrue(element.getText().equals(name.toUpperCase(Locale.ROOT)));
//				
//				// The following assert will stop the test as soon as the two names do not match
//				// Assert.assertEquals(element.getText(), name.toUpperCase(Locale.ROOT));
//				
//				if(element.getText().equals(name.toUpperCase(Locale.ROOT)))
//				{
//					// Grab handle to address text box
//					element = driver.findElement(By.xpath(".//*[@id='contact']/div[1]"));
//					
//					// Set the Actual address cell with the data from the handle
//					ExecUtil.setCellData(element.getText(), row, 3);
//					
//					// Test that the address's match
//					// Assert.assertTrue(address.equals(ExecUtil.getCellData(row,  3)));
//					
//					if(address.equals(ExecUtil.getCellData(row, 3)))
//					{
//						return true;
//					}
//				}
//		}catch(Exception e)
//		{
//			Log.info("Exception Caught and thrown, exception was : ", e);
//			// Print the stack trace to the Log4J logger
//			Log.info(e.getStackTrace());
//			Reporter.log("Exception caught and thrown, Stack Trace Follows");
//			// Report the error to TestNG reporter
//			Reporter.log(e.toString());
//			throw(e);
//		}
//	
//		return false;
//	}
//
//}
