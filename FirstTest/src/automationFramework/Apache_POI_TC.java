//package automationFramework;
//
//import java.util.Locale;
//import java.util.concurrent.TimeUnit;
//import org.apache.log4j.Logger;
//import org.apache.log4j.xml.DOMConfigurator;
//import org.openqa.selenium.By;
//import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebElement;
//import org.openqa.selenium.chrome.ChromeDriver;
//import utilities.Constant;
//import utilities.ExecUtil;
//import utilities.Utility;
//
//public class Apache_POI_TC {
//	private static WebDriver driver = null;
//	private static Logger Log = Logger.getLogger(Apache_POI_TC.class.getName());
//
//	public static void main(String[] args) throws Exception {
//
//	DOMConfigurator.configure("log4j.xml");
//		
//	// Set the path to the excel file
//    ExecUtil.setExcelFile(Constant.Excel_Data_Path,"Sheet1");
//    
//    // Setup the chrome driver
//    System.setProperty("webdriver.chrome.driver",  "C:\\Users\\jderochie\\Desktop\\chromeDriver\\chromedriver.exe");
//    driver = new ChromeDriver();
//    
//    Log.info("Log for " +  Utility.getCurrentTimeStamp());
//    
//    // Notify the log that the driver has been set up
//    if(driver != null)
//    {
//        Log.info("New driver instantiated");
//    }
//
//    // Set wait period for driver
//    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//    
//    int index = 1;
//    
//    Log.info("*******Program ready, tests beginning.*******");
//    while(ExecUtil.getCellData(index, 0) != "")
//    {
//        driver.get(Constant.URL);
//        Log.info("Website Opened Successfully");
//        
//        if(reverseLookUp(index, ExecUtil.getCellData(index, 0), ExecUtil.getCellData(index,  1), ExecUtil.getCellData(index,  2)))
//        {
//        	ExecUtil.setCellData("Pass", index, 4);
//        	Log.info(ExecUtil.getCellData(index, 1) + " Test passed.");
//        }
//        else
//        {
//        	ExecUtil.setCellData("Fail", index, 4);
//        	Log.warn(ExecUtil.getCellData(index, 1) + " Test failed.");
//        }
//        index += 1;
//    }
//    
//    Log.info("*******Program exiting safely, all tests complete.*******");
//    driver.quit();
//    
//	}
//
//	public static boolean reverseLookUp(int index, String number, String name, String address) throws Exception
//	{
//		// use the number to look up the person
//		WebElement element;
//		
//		try{
//		element = driver.findElement(By.id("c411PeopleReverseWhat"));
//		element.sendKeys(number);
//		
//		// Use the following line to incur an Exception
//		//element = driver.findElement(By.name("bobthebuilder"));
//		
//		element.submit();
//		Log.info("Phone number query submitted...");
//		
//		element = driver.findElement(By.xpath(".//*[@id='contact']/h1"));
//		
//		if(element.getText().equals(name.toUpperCase(Locale.ROOT)))
//		{
//			element = driver.findElement(By.xpath(".//*[@id='contact']/div[1]"));
//			
//			ExecUtil.setCellData(element.getText(), index, 3);
//			if(address.equals(ExecUtil.getCellData(index, 3)))
//			{
//				return true;
//			}
//		}
//		}catch(Exception e)
//		{
//			Log.warn("\n..................An exception was handled at line 85 of Apache_POI_TC.java\n");
//			Log.fatal("************************Stack Trace Begins*****************************", e);
//			Log.info(e.toString());
//			Log.info("************************Stack Trace Complete*****************************\n");
//
//			throw(e);
//		}
//		Log.warn("Address did not validate...");
//		Log.info("\nError occured at line 93 of Apache_POI_TC.java\n");
//		return false;
//	}
//}
